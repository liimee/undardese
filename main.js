if (localStorage.getItem('les') === null) localStorage.setItem('les', '[]');

var data = {
  basics1: [
    {
      type: 1,
      icon: '<div style="animation: ud .8s infinite;"><i class="bx bxs-hand-down"></i></div>',
      undardese: 'dhœ',
      meaning: 'this (noun)'
    },
    {
      type: 2,
      question: 'dhœ',
      options: [{ correct: true, val: '<i class="bx bxs-hand-down"></i>' }, { correct: false, val: '<i class="bx bx-happy"></i>' }]
    },
    {
      type: 1,
      icon: '<div style="animation: lr .8s infinite;"><i class="bx bxs-hand-right"></i></div>',
      undardese: 'adhœ',
      meaning: 'that (noun)'
    },
    {
      type: 3,
      ques: ['dhœ', 'adhœ'],
      ans: ['this (noun)', 'that (noun)']
    },
    {
      type: 4,
      question: 'adhœ',
      icon: '<i class="bx bxs-hand-down"></i>',
      ans: false
    },
    {
      type: 5
    }
  ]
}

async function course(obj) {
  var score = 0;
  var num = 0;
  for (var v of obj) {
    document.querySelector('#mainwin').style.top = 0;
    switch (v.type) {
      case 1:
        document.querySelector('#content').innerHTML = `<div id="stuff" style="animation: zi .5s">${v.icon}<div><b>${v.undardese}</b></div><div class="meaning">${v.meaning}</div></div>`;
        await addOkBtn();
        break;
      case 2:
        document.querySelector('#content').innerHTML = `<div id="stuff" style="animation: zi .5s"><b>${v.question}</b></div>`;
        var x = await askQuestion(v.options);
        if(x) score += 1;
        num += 1;
        break;
      case 3:
        var ra = Math.floor(Math.random() * (v.ques.length - 1));
        document.querySelector('#content').innerHTML = '<div id="stuff" style="animation: zi .5s"><b>' + v.ques[ra] + '</b></div>';
        var thing = [];
        v.ans.forEach((va, i) => {
          thing.push({
            correct: ra == i,
            val: va
          })
        });
        var x = await askQuestion(thing);
        if(x) score += 1;
        num += 1;
        break;
      case 4:
        var x = await askQuestion2(v.question, v.icon, v.ans);
        if(x) score += 1;
        num += 1;
        break;
      case 5:
        new Donutty(document.getElementById('content'), { max: num, value: score});
        await addOkBtn();
        exit();
    }
  }
}

function exit() {
  le = false;
  document.querySelector('#mainwin').style.top = '-105%';
}

function addOkBtn() {
  return new Promise((a, b) => {
    let g = document.createElement('button');
    g.className = 'okbtn';
    g.innerText = 'Oh!';
    g.onclick = () => {
      document.querySelector('#stuff').style.transform = 'scale(0)';
      document.querySelector('#stuff').style.animation = 'zo .5s';
      g.style.animation = 'zo .5s';
      setTimeout(() => {
        g.remove();
        a();
      }, 501);
    }
    document.querySelector('#content').appendChild(g);
  });
}

function askQuestion(a) {
  return new Promise((b, c) => {
    for (let i = a.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      let temp = a[i];
      a[i] = a[j];
      a[j] = temp;
    }
    let f = document.createElement('div');
    f.style.position = 'absolute';
    f.style.bottom = 0;
    f.style.width = '100%';
    f.style.display = 'flex';
    f.style.justifyContent = 'space-evenly';
    document.querySelector('#content').appendChild(f);
    a.forEach(v => {
      var g = document.createElement('button');
      g.className = 'btns';
      g.style.width = '100%';
      g.style.margin = '10px';
      g.innerHTML = v.val;
      g.onclick = () => {
        document.querySelector('#stuff').style.transform = 'scale(0)';
        document.querySelector('#stuff').style.animation = 'zo .5s';
        f.style.animation = 'zo .5s';
        setTimeout(() => {
          document.querySelector('#stuff').innerHTML = v.correct ? '<i style="font-size: 1.9em" class="bx bxs-check-circle"></i>' : '<i style="font-size: 1.9em" class="bx bxs-x-circle"></i>';
          document.querySelector('#stuff').style.transform = 'scale(1)';
          document.querySelector('#stuff').style.animation = 'zi .5s';
          setTimeout(() => {
            document.querySelector('#stuff').style.transform = 'scale(0)';
            document.querySelector('#stuff').style.animation = 'zo .5s';
            setTimeout(() => {
              document.querySelector('#stuff').innerHTML = '';
              document.querySelector('#stuff').style.transform = 'scale(1)';
              b((v.correct) ? true : false);
            }, 501);
          }, 1400);
          f.remove();
        }, 501);
      }
      f.appendChild(g);
    });
  });
}

window.onbeforeunload = function() {
  if (le) return 'Are you sure you want to leave?';
};

function askQuestion2(a, c, d) {
  return new Promise(b => {
    let ed = {
      a: document.createElement('div'),
      b: document.createElement('div'),
      c: document.createElement('div')
    };
    ed.a.className = 'corf';
    ed.b.id = 'stuff';
    ed.b.innerHTML = `<div><div style="font-size: 1.8em">${c}</div><div style="font-size: .4em">${a}</div></div>`;
    ed.a.appendChild(ed.b);
    let bt = {
      a: document.createElement('button'),
      b: document.createElement('button')
    };
    bt.a.className = bt.b.className = 'btns';
    bt.a.innerHTML = '<i class="bx bx-x"></i>';
    bt.a.style.backgroundColor = '#ff4242';
    bt.a.style.color = bt.b.style.color = '#fff';
    bt.b.innerHTML = '<i class="bx bx-check"></i>';
    bt.b.style.backgroundColor = '#23bc60';
    bt.a.style.width = bt.b.style.width = '100%';
    bt.a.style.margin = bt.b.style.margin = '10px';
    bt.a.onclick = () => {
      document.querySelector('#stuff').style.transform = 'scale(0)';
      document.querySelector('#stuff').style.animation = 'zo .5s';
      ed.c.style.transform = 'scale(0)';
      ed.c.style.animation = 'zo .5s';
      setTimeout(() => {
        document.querySelector('#stuff').innerHTML = (d) ? '<i class="bx bxs-x-circle"></i>' : '<i class="bx bxs-check-circle"></i>';
        document.querySelector('#stuff').style.transform = 'scale(1)';
        document.querySelector('#stuff').style.animation = 'zi .5s';
        ed.c.remove();
        setTimeout(() => {
          document.querySelector('#stuff').style.transform = 'scale(0)';
          document.querySelector('#stuff').style.animation = 'zo .5s';
          setTimeout(() => {
            document.querySelector('#content').innerHTML = '';
            b(d ? false : true);
          }, 501);
        }, 1300);
      }, 600);
    }
    bt.b.onclick = () => {
      document.querySelector('#stuff').style.transform = 'scale(0)';
      document.querySelector('#stuff').style.animation = 'zo .5s';
      ed.c.style.transform = 'scale(0)';
      ed.c.style.animation = 'zo .5s';
      setTimeout(() => {
        document.querySelector('#stuff').innerHTML = (!d) ? '<i class="bx bxs-x-circle"></i>' : '<i class="bx bxs-check-circle"></i>';
        document.querySelector('#stuff').style.transform = 'scale(1)';
        document.querySelector('#stuff').style.animation = 'zi .5s';
        ed.c.remove();
        setTimeout(() => {
          document.querySelector('#stuff').style.transform = 'scale(0)';
          document.querySelector('#stuff').style.animation = 'zo .5s';
          setTimeout(() => {
            document.querySelector('#content').innerHTML = '';
            b(d ? true : false);
          }, 501);
        }, 1300);
      }, 600);
    }
    ed.c.appendChild(bt.a);
    ed.c.appendChild(bt.b);
    ed.c.style.position = 'absolute';
    ed.c.style.bottom = 0;
    ed.c.style.width = '100%';
    ed.c.style.display = 'flex';
    ed.c.style.justifyContent = 'space-evenly';
    document.querySelector('#content').innerHTML = '';
    document.querySelector('#content').appendChild(ed.a);
    document.querySelector('#content').appendChild(ed.c);
    document.querySelector('#stuff').style.animation = 'zi .5s';
  })
}